import React from 'react';
import './App.css';
import { Main } from './components/Main/Main';
import { store } from './components/Shared/Store/index';
import { Provider } from 'react-redux';


class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

export default App;
