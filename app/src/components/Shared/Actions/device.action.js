import { Api } from './../Api/Api';
import { DeviceConstants } from './../Reducers/device.reducer'

export const DeviceActions = {
    getDevices,
    updateDevice
}

function updateDevice(id, data) {
    return dispatch => {
        return Api.updateDevice(id, data).then((resp) => resp.json()).then(
            (response) => {
                dispatch(updatedDevices(response.data));
            },
            (error) => {
                dispatch(errorHandle(error));
            }
        )
    }
}
function getDevices() {
    return dispatch => {
        return Api.getDevices().then((resp) => resp.json()).then(
            (response) => {
                console.log(response.data);
                dispatch(allDevices(response.data));
            },
            (error) => {
                dispatch(errorHandle(error));
            }
        )
    }
}

function allDevices(data) {
    return {
        type: DeviceConstants.ALL_DEVICES,
        data
    }
}

function updatedDevices(data) {
    return {
        type: DeviceConstants.UPDATE_DEVICE,
        data
    }
}

function errorHandle(error) {
    return {
        type: DeviceConstants.ERROR,
        error
    }
}

