import { createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { devices } from './../Reducers/device.reducer'

//Log all the state and dispatch
const loggerMiddleWare = createLogger();

const middleWares = [thunk];

middleWares.push(loggerMiddleWare); // Add the Logger

export const store = createStore(
   devices,
   applyMiddleware(
       ...middleWares
   )
);