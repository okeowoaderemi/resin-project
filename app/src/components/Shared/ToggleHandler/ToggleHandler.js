import React from 'react';
import Toggle from 'react-toggle';

/**
 * Purpose of this class render a toggle which accepts Objects
 */
export default class ToggleHandler extends React.Component {
    constructor(props){
        super(props);
        this.onChange = this.onChange.bind(this);
        
    }
    onChange(event){
        this.props.onChange.call(this.props.value[0],event, this.props.value[1]);
    }
    render() {
        return (
            <Toggle onChange={this.onChange} style={this.props.style} icons={false} defaultChecked={this.props.defaultChecked} />
        );
    }
}