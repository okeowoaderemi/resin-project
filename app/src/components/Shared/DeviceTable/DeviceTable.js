import React from 'react';
import { Table, Container } from 'rendition';
import 'react-toggle/style.css';
import './DeviceTable.css';
import ToggleHandler from '../ToggleHandler/ToggleHandler';
import { DeviceActions } from './../Actions/device.action';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';



const StatusText = (props) => <span>{(props.value) ? 'On' : 'Off'}</span>

export class DeviceTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount() {
        this.props.getDevices()
    }
    columns = [
        {
            field: 'name',
            sortable: true,
            label: 'Name',
            render: value => <strong>{value}</strong>
        }, {
            field: 'active',
            label: 'State',
            render: (value, row) => {
                return (
                    (<div>
                        <ToggleHandler defaultChecked={value} value={[this,row]} onChange={this.handleChangeState} style={{ margin: 2 }} /> <StatusText value={value} />
                    </div>
                    ));
            }
        }, {
            field: 'brightness',
            label: 'Brightness',
            render: value => <p style={{ margin: '0' }}>{value}%</p>
        }
    ];




    handleChangeState(key, row) {
       row.active = (!row.active);
       this.props.updateDevice(row.id,row);
    }

    render() {
        return (
            <Container>
                <Table columns={this.columns} data={this.props.data} />
            </Container>
        );
    }
}

const mapStateToProps = state => {

    const data = state.devices_reducer.data;
    return {
        data: data
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getDevices: bindActionCreators(DeviceActions.getDevices, dispatch),
        updateDevice: bindActionCreators(DeviceActions.updateDevice,dispatch)
    }
}
const AppContainer = connect(mapStateToProps, mapDispatchToProps)(DeviceTable);
export default AppContainer;