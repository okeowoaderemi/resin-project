import { combineReducers } from "redux";

// Declare the constants

export const DeviceConstants = {
    ALL_DEVICES: 'ALL_DEVICES',
    GET_DEVICE: 'GET_DEVICE',
    UPDATE_DEVICE: 'UPDATE_DEVICE',
    ERROR: 'ERROR'
}

const defaultState = { data: [], fetching: false }

let devices_reducer = (state = defaultState, action) => {
    switch (action.type) {
        case DeviceConstants.ALL_DEVICES:
            return {
                ...defaultState,
                data: action.data,
                fetching: true
            }
            break;
        // case DeviceConstants.UPDATE_DEVICE:
        //     return {
        //         ...defaultState,
        //         data: action.data,
        //         fetching:true
        //     }
        //     break;
        case DeviceConstants.ERROR:
            return action.error;
            break;
        default:
            return state;
    }
}


export const devices = combineReducers({ devices_reducer });