import { constant } from './constant';
const baseUrl = constant.baseUrl;
export const Api = {
    getDevices() {
        return fetch(`${baseUrl}/device`);
    }
    ,
    getDevice(id) {
        return fetch(`${baseUrl}/device/${id}`);
    },
    updateDevice(id, data) {
        return fetch(`${baseUrl}/device/${id}`, {
            method: 'PATCH',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                data
            })
        });
    }
};