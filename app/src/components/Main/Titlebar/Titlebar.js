import React from "react";
import './Titlebar.css';



export default class Titlebar extends React.Component {

    render() {
        return (
            <div className="Titlebar">
                <div className="homeBar">
                    <i className="fa fa-home fa-2"></i>
                </div>
                <div className="backBar">
                    <i className="fa fa-chevron-left"></i> Next
                </div>
                <div className="textBar">
                <i className="fa fa-lightbulb-o fa-2x"></i> <span className="text">Lighting</span>
                </div>
                <div className="clearfix"></div>
            </div>

        );
    }
}