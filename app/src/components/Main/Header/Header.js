import React from 'react';
import { Box, Flex } from 'rendition';
import './Header.css';



export default class Header extends React.Component {
  
    render() {
        return (
            <Box className="header" px={2} py={2} bg='#222222' color='#9A9A9A' >
                <Flex flex='wrap'>
                    <Box px={2} py={2} width={1 / 3} className="left-title">

                        {this.props.leftText}
                    </Box>
                    <Box px={2} py={2} width={1 / 3} className="middle-title">

                        {this.props.middleText}
                    </Box>
                    <Box px={2} py={2} width={1 / 3} className="right-title">
                      
                        {this.props.rightText}
                    </Box>
                </Flex>
            </Box>


        );
    }
}