import React from "react";
import { Flex, Box } from "rendition";
import Header from './Header/Header';
import DeviceTable from "../Shared/DeviceTable/DeviceTable";
import Titlebar from "./Titlebar/Titlebar";




export class Main extends React.Component {

 

    render() {

        return (
            <Flex width='100%'>
                <Box width='100%'>
                    <Header leftText={this.getDate()} middleText="11:20 AM" rightText="Name Surname" />
                    <Titlebar />
                    <Box>
                        <Flex flex='wrap'>
                            <Box width={1 / 2} px={2} py={2}>
                                <DeviceTable />
                            </Box>
                            <Box width={1 / 2} px={2} py={2}>
                                Darkness
                </Box>
                        </Flex>
                    </Box>
                </Box>
            </Flex>
        );

    }

    getDate() {
        const Months = ["January", "Febuary", "March", "April", "May", "July", "August", "October", "November", "December"];

        const weekday = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
        let date = new Date();
        return `${weekday[(date.getDay() - 1)]}  ${date.getDate()} ${Months[(date.getMonth() - 1)]}, ${date.getFullYear()}`;
    }


}