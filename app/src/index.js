import React from 'react';
import ReactDOM from 'react-dom';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';
import App from './App';
import { Provider } from "rendition";


ReactDOM.render(
    <Provider>
        <App />
    </Provider>
    , document.getElementById('root'));

